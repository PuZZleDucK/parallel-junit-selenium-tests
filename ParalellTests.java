

import org.openqa.selenium.*;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.*;
import java.util.*;
import java.util.stream.*;
import java.util.stream.Collectors.*;
import org.junit.rules.*;
import static org.junit.Assert.*;
import static org.junit.Assume.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.*;
import org.junit.runners.Suite.SuiteClasses;
import org.junit.runner.*;
import org.junit.runners.*;
import com.googlecode.junittoolbox.ParallelRunner;


@RunWith(ParallelRunner.class)
    public class ParalellTests {
    private int windowWidth = 400;
    private int windowHeight = 400;
    private int[][] locations = {{0*windowWidth,0*windowHeight},
                               {1*windowWidth,0*windowHeight},
                               {0*windowWidth,1*windowHeight},
                               {1*windowWidth,1*windowHeight},
                               {0*windowWidth,2*windowHeight},
                               {1*windowWidth,2*windowHeight}};
    private int CTRL=0;
    private int KEY_W=1;
    private int KEY_T=2;
    private int xCTRL=3;
    private List<WebDriver> drivers = new ArrayList<WebDriver>();
    private By aTag = By.tagName("a");



    public static void main(String[] args) {
        System.out.println("PARALELL: \n======================================");
        org.junit.runner.JUnitCore.main("ParalellTests");
    }
    
    @Test
    public void testA() {
        System.setProperty("webdriver.chrome.driver", "/usr/lib/chromium-browser/chromedriver");
        System.out.println("\n\nCHROME");
        WebDriver chrome = setupDriver(new ChromeDriver(), "http://www.sportsbet.com.au/", 0);
        Action[] cActions = setupShortcuts(chrome);

        try{
            Thread.sleep(15000);
        } catch (Exception e) { System.out.println("E: "+e); }

        List<WebElement> linksC = chrome.findElements(aTag);
        System.out.println("LINKS:\n=========================");
        chrome.quit();
    }
    
    @Test
    public void testB() {
        System.setProperty("webdriver.chrome.driver", "/usr/lib/chromium-browser/chromedriver");
        System.out.println("\n\nCHROME");
        WebDriver chrome = setupDriver(new ChromeDriver(), "http://www.sportsbet.com.au/", 1);
        Action[] cActions = setupShortcuts(chrome);

        try{
            Thread.sleep(15000);
        } catch (Exception e) { System.out.println("E: "+e); }

        List<WebElement> linksC = chrome.findElements(aTag);
        System.out.println("LINKS:\n=========================");
        chrome.quit();
    }
    
    
    @Test
    public void testD() {
        System.out.println("[FF]");
        WebDriver firefox = setupDriver(new FirefoxDriver(), "http://www.sportsbet.com.au/", 3);
        Action[] fActions = setupShortcuts(firefox);
        String targetSite = "http://www.sportsbet.com.au/";
        
        try{
            Thread.sleep(15000);
        } catch (Exception e) { System.out.println("E: "+e); }
        
        List<WebElement> linksF = firefox.findElements(aTag);
        System.out.println("FFox link count: " + linksF.size());
        firefox.quit();  
    }
    
    @Test
    public void testE() {
        System.out.println("[FF]");
        WebDriver firefox = setupDriver(new FirefoxDriver(), "http://www.sportsbet.com.au/", 4);
        Action[] fActions = setupShortcuts(firefox);
        String targetSite = "http://www.sportsbet.com.au/";
        
        try{
            Thread.sleep(15000);
        } catch (Exception e) { System.out.println("E: "+e); }
        
        List<WebElement> linksF = firefox.findElements(aTag);
        System.out.println("FFox link count: " + linksF.size());
        firefox.quit();  
    }
    



    private WebDriver setupDriver(WebDriver driver, String target, int position) {
//RUN_ONCE_SETUP
      System.out.println("-driver setup");
      driver.manage().window().setSize(new Dimension(windowHeight, windowWidth));
      driver.manage().window().setPosition(new Point(locations[position][0], locations[position][1]));
      driver.get(target);
      return driver;
    }
    
    private Action[] setupShortcuts(WebDriver driver) {
      Actions cbuilder = new Actions(driver);
      Action[] actions = new Action[4];
      actions[CTRL] = cbuilder.keyDown(Keys.CONTROL).build();
      actions[KEY_W] = cbuilder.sendKeys("w").build();
      actions[KEY_T] = cbuilder.sendKeys("\t").build();
      actions[xCTRL] = cbuilder.keyUp(Keys.CONTROL).build();
      return actions;
    }
    
    
}//class
